export class LoginPage{
    // login_username_input='input[type=email]'
    // login_password_input='input[type=password]'
    // login_button='.euiButtonContent'

    login_email_input='.euiFieldText'
    login_password_input='.euiFieldPassword'
    login_button='.euiButtonContent'

    enterEmail(email){
        cy.get(this.login_email_input).type(email)
    }
    clearLoginFields(){
        cy.get(this.login_email_input).clear()
        cy.get(this.login_password_input).clear()
    }
    enterPassword(password){
        cy.get(this.login_password_input).type(password)
    }
 
    submitLogin(){
        cy.get(this.login_button).click()
    }
}