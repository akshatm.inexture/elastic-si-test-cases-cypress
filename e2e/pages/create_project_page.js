export class CreateProjectPage{

    create_project_name='input[name=name]'
    create_project_version='input[name=version]'
    create_project_description='textarea'
    create_project_type='[name=search_type]'
    create_project_search_mode='[name=index_mode]'
    create_project_select_engine='[name=master_template]'

    submit_createForm_id='#project_save'


    findCardTitle(value){
        cy.contains(value).click()
    }
    //Create_Form  Submit Button
    submit_CreateForm(){
        cy.get(this.submit_createForm_id).click()
    }

    //Name Field
    checkFirstNameField(nameValue){
      cy.get(this.create_project_name).clear()
      cy.get(this.create_project_name).type(nameValue)
      this.submit_CreateForm()
    }

    //Version Field
    checkVersionField(versionValue){
        cy.get(this.create_project_version).clear()
        cy.get(this.create_project_version).type(versionValue)
        this.submit_CreateForm()
    }

    //Type Field
    checkTypeField(select_typeValue,type_OptionValue){
        cy.get(this.create_project_type).select(select_typeValue).should('have.value',type_OptionValue)
    }

    //Search Mode Field
    checkSerachModeField(select_searchModeValue,searchMode_OptionValue){
        cy.get(this.create_project_search_mode).select(select_searchModeValue).should('have.value',searchMode_OptionValue)
    }

    //Select Engine Field
     checkSelectEngineField(select_selectEngineValue,selectEngine_OptionValue){
        cy.get(this.create_project_select_engine).select(select_selectEngineValue).should('have.value',selectEngine_OptionValue)
     }


    //Description Field
    checkDescriptionField(describeValue){
        cy.get(this.create_project_description).clear()
        cy.get(this.create_project_description).type(describeValue)
        this.submit_CreateForm()
    }

    
}