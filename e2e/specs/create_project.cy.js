/// <reference types="cypress" />
import { LoginPage } from "../pages/login_page";
import { CreateProjectPage } from "../pages/create_project_page";

const loginPage = new LoginPage();
const createProjectPage = new CreateProjectPage();

describe("Create Project Test Cases", () => {
  describe("Login Activity Test Cases", () => {
    it("Go to Login Page", () => {
      cy.visit("https://search-admin-dev-mamb5phriq-uc.a.run.app/auth/");
    });

    it("Successfull Login Completed", () => {
      loginPage.enterEmail("admin@weblinktechs.com");
      loginPage.enterPassword("!adg@u^s5FE43q");
      loginPage.submitLogin();
    });
  });

  describe("Get Started Page", () => {
    it("Get Started-Create New Project", () => {
      createProjectPage.findCardTitle("Create Project");
      // createProjectPage.findCardTitle('Create Usecase')
      //  createProjectPage.findCardTitle('Add New List')
    });
    it("Create New Click", () => {
      createProjectPage.findCardTitle("Create New");
    });
  });

  describe("Create Project Form Test Cases", () => {
    describe("First Name Field", () => {
      it("Project Name--Empty Field", () => {
        createProjectPage.submit_CreateForm();
      });
      it("Project Name--Special Characters Not Allowed", () => {
        createProjectPage.checkFirstNameField("test$123@");
      });
      it("Project Name-- Spaces Not Allowed", () => {
        createProjectPage.checkFirstNameField("tes t  123");
      });
      it("Project Name--should be Alphanumeric", () => {
        createProjectPage.checkFirstNameField("test123");
      });
      it("Project Name--should be Numeric", () => {
        createProjectPage.checkFirstNameField("123456");
      });
      it("Project Name--should be Characters", () => {
        createProjectPage.checkFirstNameField("testdemo");
      });
    });
    describe("Version Field", () => {
      it("Empty Version Field", () => {
        createProjectPage.submit_CreateForm();
      });
      it("Version should not a String", () => {
        createProjectPage.checkVersionField("a.b.c");
      });
      it("Version should not a x.x Format", () => {
        createProjectPage.checkVersionField("1.2");
      });
      it("Version should be Number with x.x.x Format", () => {
        createProjectPage.checkVersionField("1.2.2");
      });
    });

    describe("Select Menus with different Options Test Cases", () => {
      describe("Select Project Type", () => {
        
        //Single Project Combination Test Cases
        describe("Single Project Field Test Cases", () => {
          it("Single Project Selected", () => {
            createProjectPage.checkTypeField("Single Project", "SINGLE");
          });

          describe("Pretune Search Relevance Field(Advanced)", () => {
            it("Pretune Search Relevance Field Seleted as a value", () => {
              createProjectPage.checkSerachModeField(
                "Pretune Search Relevance",
                "ADVANCED"
              );
            });
            describe("Pretune Selected--->Search Engine Options Check", () => {
              it("Custom Relevance Engine(Disable)", () => {});
              it("Ranking Based Engine(Version-2)", () => {});
            });
          });
          describe("Custom Search Relevance Field (Basic)", () => {
            it("Custom Search Relevance Seleted as a value", () => {
              createProjectPage.checkSerachModeField(
                "Custom Search Relevance",
                "BASIC"
              );
            });
            describe("Custom Search Selected--->Search Engine Options Check", () => {
              it("Custom Relevance Engine(Enable)", () => {});
              it("Ranking Based Engine", () => {});
            });
          });
        });

        //Meta Project Combination Test Cases
        describe("Meta Project Test Cases", () => {
          it("Meta Project Selected", () => {
            createProjectPage.checkTypeField("Meta Project", "META");
          });

          describe("Pretune Search Relevance Field(Advanced)", () => {
            it("Custom Relevance Engine(Disable)", () => {});
            it("Ranking Based Engine(Version-2)", () => {});
          });
        });
      });
    });

    // describe("Description Field", () => {
    //   it("Empty Description Field", () => {
    //     createProjectPage.submit_CreateForm();
    //   });
    //   it("Filled Description Field", () => {
    //     createProjectPage.checkDescriptionField(
    //       "This is Create Project Description Field of Elastic SI Project with Name-test and version-1.2.2"
    //     );
    //   });
    // });
  });
});
