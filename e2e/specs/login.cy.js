/// <reference types="cypress" />
import { LoginPage } from "../pages/login_page";

const loginPage=new LoginPage()

describe('Elastic-SI Login Test Cases',()=>{
    it('Go to Login Page',()=>{
        cy.visit('https://search-admin-dev-mamb5phriq-uc.a.run.app/auth/')
    })

    it('Email is a Required Field',()=>{
        loginPage.clearLoginFields()
        loginPage.enterPassword('!adg@u^s5FE43q')
        loginPage.submitLogin()
        // cy.reload()
    })
    it('Password is a Required Field',()=>{
        loginPage.clearLoginFields()
        loginPage.enterEmail('admin@weblinktechs.com')
        loginPage.submitLogin()
        // cy.reload()
    })
    it('Email and Password as a Required Field',()=>{
        loginPage.clearLoginFields()
        // loginPage.enterEmail('test@test.com')
        // loginPage.enterPassword('admin123')
        loginPage.submitLogin()
        // cy.reload()
    })

    it('Invalid Email and Invalid Password',()=>{
        loginPage.clearLoginFields()
        loginPage.enterEmail('adminweblinktechs.com')
        loginPage.enterPassword('swqag@u^s5FE43q')
        loginPage.submitLogin()
        // cy.reload()
    })

    it('Invalid Email and Valid Password',()=>{
        loginPage.clearLoginFields()
        loginPage.enterEmail('adminweblinktechs.com')
        loginPage.enterPassword('adg@u^s5FE43q')
        loginPage.submitLogin()
        // cy.reload()
    })

    it('Valid Email and Invalid Password',()=>{
        loginPage.clearLoginFields()
        loginPage.enterEmail('admin@weblinktechs.com')
        loginPage.enterPassword('swqag@u^s5FE43q')
        loginPage.submitLogin()
        // cy.reload()
    })
       
    it('Valid Email and Valid Password',()=>{
        loginPage.clearLoginFields()
        loginPage.enterEmail('admin@weblinktechs.com')
        loginPage.enterPassword('!adg@u^s5FE43q')
        loginPage.submitLogin()
    })
})